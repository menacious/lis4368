> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Daniel A. Meneses

### Project 2 Requirements:

*Four parts:*

1. Completing CRUD (Create, Read, Update, Delete) functionality introduced in Assignments 4 and 5
2. Using JSTL to prevent Cross-Site Scripting (XSS)
3. Implementing the option to display data, add, update, and delete entries directly from the webpage
4. Questions (Chs. 16, 17)

#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of passed data validation
* Screenshot of data being displayed on site
* Screenshot of modify form and modified data
* Screenshot of delete confirmation prompt
* Screenshot of associated database changes (select, insert, update, delete)

#### Assignment Screenshots:

*Screenshot of valid user form entry*:

![Valid Entry Screenshot](img/valid_data.png)

*Screenshot of passed validation*:

![Passed Validation Screenshot](img/passed_val.png)

*Screenshot of Displayed Data*:

![Display Data Screenshot](img/show_data.png)

*Screenshot of Modify Form*:

![Modify Form Screenshot](img/modify.png)

*Screenshot of Modified Data*:

![Modified Data Screenshot](img/show_modify.png)

*Screenshot of Delete Warning*:

![Delete Warning Screenshot](img/delete_prompt.png)

*Screenshot of associated data changes*:

![Database Changes Screenshot](img/mysql_changes.png)
