> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Daniel Meneses

### Assignment 2 Requirements:

*Five Parts:*

1. MySQL Installation
2. Java Servlet Compilation
3. Data-driven website creation
4. Chapter Questions (Chs 5, 6)
5. Skillsets 1 - 3

#### README.md file should include the following items:

* Assessment links
* Screenshot of querybook.html
* Screenshot of the query results
* Screenshot of A2 index.jsp file
* Screenshots of each Java skillset

#### Assessment Links:

1. http://localhost:9999/hello
2. http://localhost:9999/hello/HelloHome.html
3. http://localhost:9999/hello/sayhello (invokes HelloServlet)
4. http://localhost:9999/hello/querybook.html

#### Assignment Screenshots:

*Screenshot of querybook.html*:

![querybook.html Screenshot](img/database_connectivity1.png)

*Screenshot of query results from querybook.html*:

![Query Results Screenshot](img/database_connectivity2.png)

*Screen Recording of A2 index.jsp file*:

![A2 index.jsp Screenshot](img/lis4368_a2_index.gif)

### Skillset Screenshots:

*Screenshot of Skillset 1 - System Requirements*:

![Skillset 1 Screenshot](img/skillset1.png)

*Screenshot of Skillset 2 - Looping Structures*:

![Skillset 2 Screenshot](img/skillset2.png)

*Screenshot of Skillset 3 - Number Swap*:

![Skillset 3 Screenshot](img/skillset3.png)