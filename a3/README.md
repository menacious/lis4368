> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Daniel A. Meneses

### Assignment 3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records in each table)
3. Provide Bitbucket read-only access to repo (Language SQL), _must_ include README.md, using Markdown syntax, and include links to _all_ of the following files (from README.md):
    - docs folder: a3.mwb, and a3.sql
    - img folder: a3.png (export a3.mwb file as a3.png)
    - README.md (MUST display a3.png ERD) 

#### README.md file should include the following items:

* Screenshot of A3 ERD that links to the image
* Screenshot of populated tables
* Screenshot of a3/index.jsp
* Links to a3.mwb and a3.sql


#### Assignment Screenshot and Links: 
 
*Screenshot A3 ERD*: 
 
[![A3 ERD](img/a3.png "ERD based upon A3 Requirements")](img/a3.png)

*Screenshots of populated tables*:

![A3 Pet Store Table](img/a3_petstore.png "Pet Store Table with 10 Records")

![A3 Customer Table](img/a3_customer.png "Customer Table with 10 Records")

![A3 Pet Table](img/a3_pet.png "Pet Table with 10 Records")

*Screenshot of A3 index.jsp*:

![A3 index.jsp](img/a3_index_jsp.png "a3/index.jsp with Screenshots and Links")
 
*A3 docs: a3.mwb and a3.sql*: 
 
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format") 
 
[A3 SQL File](docs/a3.sql "A3 SQL Script")

### Skillset Screenshots:

*Skillset 4 Screenshot*:

![Skillset 4 Screenshot](img/skillset4.png "Skillset 4: Directory Info")

*Skillset 5 Screenshot*:

![Skillset 5 Screenshot](img/skillset5.png "Skillset 5: Character Info")

*Skillset 6 Screenshot*:

![Skillset 6 Screenshot](img/skillset6.png "Skillset 6: Determine Character")

