> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Daniel A. Meneses

### Assignment 5 Requirements:

*Four parts:*

1. Expanding upon the server-side validation performed in Assignment 4
2. Using JSTL to prevent Cross-Site Scripting (XSS)
3. Adding insert functionality to A4 to populate an existing database
4. Questions (Chs. 11, 12)

#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of passed data validation
* Screenshot of associated database entry
* Skillsets 13 - 15 Screenshots

#### Assignment Screenshots:

*Screenshot of valid user form entry*:

![Valid Entry Screenshot](img/a5_valid.png)

*Screenshot of passed validation*:

![Passed Validation Screenshot](img/a5_passed.png)

*Screenshot of associated data entry*:

![Database Entry Screenshot](img/a5_insert.png)


#### Skillset Screenshots

*Screenshot of Skillset 13*:

![Skillset 13 Screenshot](img/skillset13.gif)

*Screenshot of Skillset 14*:

![Skillset 14 Screenshot](img/skillset14.gif)

*Screenshot of Skillset 15*:

![Skillset 15 Screenshot](img/skillset15.png)
