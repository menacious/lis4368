> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Daniel A. Meneses

### Assignment 4 Requirements:

*Three parts:*

1. Using servlets to perform server-side data validation
2. Utilizing Model View Controller (MVC) framework to implement web application
3. Questions (Chs. 11, 12)

#### README.md file should include the following items:

* Screenshot of failed data validation
* Screenshot of passed data validation
* Skillsets 10 - 12 Screenshots

#### Assignment Screenshots:

*Screenshot of failed validation*:

![Failed Validation Screenshot](img/failed_val.png)

*Screenshot of passed validation*:

![Passed Validation Screenshot](img/passed_val.png)

#### Skillset Screenshots

*Screenshot of Skillset 10*:

![Skillset 10 Screenshot](img/skillset10.png)

*Screenshot of Skillset 11*:

![Skillset 11 Screenshot](img/skillset11.png)

*Screenshot of Skillset 12*:

![Skillset 12 Screenshot](img/skillset12.png)
