> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Daniel A. Meneses

### Project 1 Requirements:

*Deliverables:*

1. Client-side data validation using jQuery and regular expressions
2. Screenshots of failed and passed validation
3. Questions (Chs. 9, 10)

#### README.md file should include the following items:

* Screenshots of failed and passed validation
* Screenshots of Java skillsets
* Link to main Bitbucket repo

#### Assignment Screenshots and Links:

*Screenshot of failed validation*:

![JQuery Failed Validation Screenshot](img/failed_validation.png)

*Screenshot of passed validation*:

![JQuery Passed Validation Screenshot](img/passed_validation.png)

*Main Bitbucket Repo*:

[LIS4368 Bitbucket Repo Link](https://bitbucket.org/menacious/lis4368/src/master/ "Bitbucket Repo Link")

#### Skillset Screenshots:

*Skillset 7 Screenshot(s)*:

![Skillset 7 Screenshot](img/skillset7.png "Skillset 7: Count Characters")

*Skillset 8 Screenshot(s)*:

![Skillset 8 Screenshot](img/skillset8a.png "Skillset 8: ASCII (Part A)")

![Skillset 8 Screenshot](img/skillset8b.png "Skillset 8: ASCII (Part B)")

*Skillset 9 Screenshot(s)*:

![Skillset 9 Screenshot](img/skillset9.png "Skillset 9: Grade Calculator")
