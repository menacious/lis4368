> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications Development

## Daniel Meneses

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - MySQL Installation
    - Java Servlet Compilation
    - Data-driven website creation
    - Provide screenshots of database connectivity

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create an Entity Relationship Diagram (ERD) using MySQL Workbench
    - Include data (at least 10 records per table)
    - Provide screenshots of populated tables
    - Provide links to .mwb and .sql files
    - Include a picture of the ERD that links to the original image file

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Compiling servlets to perform server-side data validation
    - Use of Model View Controller (MVC) framework to implement web application
    - Provide screenshots of failed validation
    - Display passed validation page

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Expanding upon the server-side validation performed in Assignment 4
    - Using JSTL to prevent Cross-Site Scripting (XSS)
    - Adding insert functionality to A4 to populate an existing database
    - Provide screenshots of insert functionality

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Use JQuery and regular expressions to perform client-side data validation
    - Edit Java Server Page file to include additional query fields
    - Provide screenshots of failed and passed validations

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Completing CRUD (Create, Read, Update, Delete) functionality introduced in Assignments 4 and 5
    - Using JSTL to prevent Cross-Site Scripting (XSS)
    - Implementing the option to display data, add, update, and delete entries directly from the webpage

